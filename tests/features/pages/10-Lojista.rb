class   Lojista < Request

    def altera_seguro(autenticacao)

        seguro = exec_get("/api/lojista/alteraSeguro",autenticacao)

        return seguro
    end 

    def lojista_operador(autenticacao)

        operador = exec_get("/api/lojista/listaLojistaOperador",autenticacao)

        return operador
    end

    def proposta_agente(autenticacao)

        agente = exec_get("/api/lojista/listarLojistaPropostaAgente",autenticacao)

        return agente
    end
end

