require 'time'
require_relative '../pages/geradorrandomico.rb'

class Util < GeradorRandomico

    def initialize
       #$usuario = "CRPAPP"
        $usuario = "331LUANA"
        $senha = "M327IZ3Z"
        $autenticacao = ""
        criar_pasta_log
    end

    def gerar_data_epoch(dia)
        data = []
        dataini = Time.new * 1000
        datafin = (dataini + dia.to_i * 86400) * 1000 

        data <<  Time.parse(dataini.to_s).to_i
        data <<  Time.parse(datafin.to_s).to_i
        return data
    end

    def criar_pasta_log
        @diretorio = "C:/report_automacao"
        Dir.mkdir(@diretorio) unless File.exists?(@diretorio)

        @diretorio = "#{Dir.pwd}/reports"
        Dir.mkdir(@diretorio) unless File.exists?(@diretorio)
    end

    def gravar_request_response(path,request,response)
        arquivo = "C:/report_automacao/retorno_api_#{DateTime.now.strftime("%d_%m_%Y-%H_%M")}.txt"

        arquivo = File.new(arquivo, "w")
        File.write(arquivo, "Servico: #{path}\n\n\nRequisição: #{request}\n\n\nRetorno: #{response}")
        arquivo.close
        
        arquivo2 = "#{Dir.pwd}/retorno_api_#{DateTime.now.strftime("%d_%m_%Y-%H_%M")}.txt"
        arquivo2 = File.new(arquivo2, "w")
        File.write(arquivo2, "Servico: #{path}\n\n\nRequisição: #{request}\n\n\nRetorno: #{response}")
        arquivo2.close
    end

    def gravar(request_return)
        arquivo = "C:/report_automacao/#{DateTime.now.strftime("%d_%m_%Y-%H_%M")}.txt"
        arquivo = File.new(arquivo, "w")
        File.write(arquivo, "#{request_return.to_json.force_encoding("UTF-8")}")
        arquivo.close
    end
end