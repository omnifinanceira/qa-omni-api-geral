class CidadeUf < Request

    def listar_cidade_uf(autenticacao, uf)

        listarcidadeuf = exec_get("/api/cidade/listCidadeByUf/#{uf}",autenticacao)

        return listarcidadeuf
    end
end