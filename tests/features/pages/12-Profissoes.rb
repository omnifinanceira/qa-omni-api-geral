class Profissoes < Request

    def lista_profissoes(autenticacao)

        profissoes = exec_get("/api/profissao/list",autenticacao)
  
        return profissoes
    end

    def lista_profissoes_id(autenticacao, id)

        profissoes = exec_get("/api/profissao/listarProfissoesPorClasse/#{id}",autenticacao)
        
        return profissoes
    end
end   