class Autenticacao < Request

    def fazer_autenticacao(usuario = $usuario,senha = $senha)

        body = {
            "p_nome": usuario,
            "p_senha": senha
        }

        autenticacao = exec_post_aut(body)
        # {"nome"=>"331LUANA", "emp"=>"331", "ide"=>"56489761", "cpf"=>"26261705852", "codMensagem"=>"OK", "mensagem"=>"pck_login.prc_seleciona_sistema"}
        return autenticacao
    end

    def ws_auth_user(usuario = $usuario,senha = $senha)
        body = {
            "usuarioTO": {
                "plataforma": "WEB",
                "versaoAplicacao": "2.4.0",
                "idCodAplicacao": "1",
                "login": "#{usuario}",
                "senha": "#{senha}",
                "tokenPush": ""
            }
        }

        autenticacao = exec_post_aut_ws(body)
    end
end