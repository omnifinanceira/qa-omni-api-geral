Quando('eu preencher o campo {string}') do |usuario|
  @aut = Autenticacao.new
  @usuario = usuario
end

Quando('o campo {string}') do |senha|
  @senha = senha
end

Quando('faco a requisicao') do
  @response_login = @aut.fazer_autenticacao(@usuario,@senha)
end

Quando('faco a requisicao do ws auth user') do
  @response_login = @aut.ws_auth_user(@usuario,@senha)
end

Entao('valido o retorno da autenticacao de agente unico') do
  @response_login.each { |row| expect(!row.nil?).to be_truthy }
end

Entao('valido o retorno da autenticacao de mais de um agente') do
  @response_login.each { |row| expect(!row.nil?).to be_truthy }
end

Dado('que eu autentico') do
  aut = Autenticacao.new
  $autenticacao = aut.ws_auth_user
end