Dado('faco a requisicao para tipo assistencia') do
    tipoAssistencias = TipoAssistencias.new
    @listar_tipos_assistencias = tipoAssistencias.listar_tipos_assistencias($autenticacao)
end

Entao('valido o retorno tipo assistencia') do
    expect(@listar_tipos_assistencias.code).to eq(200)
end
          
