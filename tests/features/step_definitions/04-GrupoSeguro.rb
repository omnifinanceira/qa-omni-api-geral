Quando('faco a requisicao para Grupo seguro' ) do
    grupo_seguro = GrupoSeguro.new
    @listar_grupo_seguro = grupo_seguro.listar_grupo_seguro($autenticacao)
end

Entao('valido o retorno Grupo seguro') do
    expect(@listar_grupo_seguro.code).to eq(200)
end

