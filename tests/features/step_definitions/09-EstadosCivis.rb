Dado('faco a requisicao do estado civil') do
   estadoCivil = EstadosCivis.new  
   @lista_de_estados_civis = estadoCivil.lista_de_estados_civis($autenticacao)
end

Entao('valido o retorno do estado civil') do
    expect(@lista_de_estados_civis.code).to eq(200)
end
