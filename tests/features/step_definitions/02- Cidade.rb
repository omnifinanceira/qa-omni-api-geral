Quando('faco a requisicao para cidade passando o UF') do
    cidadeuf = CidadeUf.new
    @lista_cidade = cidadeuf.listar_cidade_uf($autenticacao,"SP")
end

Entao('valido o retorno da cidade passando o UF') do
    expect(@lista_cidade.code).to eq(200)
end 