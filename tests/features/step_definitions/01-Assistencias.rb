Quando('faco a requisicao para listar os assistencia') do
    @assistencia = Assistencias.new
    @response_assistencia = @assistencia.listar_assistencias($autenticacao)
end

Entao('valido o retorno do servico de assistencia') do
    expect(@response_assistencia.code).to eq(200)
end