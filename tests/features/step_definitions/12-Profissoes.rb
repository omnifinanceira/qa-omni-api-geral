Dado('faco a requisicao das profissoes') do
    profissoes = Profissoes.new  
    @lista_profissoes = profissoes.lista_profissoes($autenticacao) 
end
  
Entao('valido o retorno das profissoes') do
    expect(@lista_profissoes.code).to eq(200)
end

Dado('faco a requisicao das profissoes com id') do
    profissoes = Profissoes.new  
    @lista_profissoes = profissoes.lista_profissoes_id($autenticacao, "1") 
end
  
Entao('valido o retorno das profissoes com id') do
    expect(@lista_profissoes.code).to eq(200)
end  