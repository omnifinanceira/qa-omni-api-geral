Quando('faco a requisicao do servico tipos de combustivel') do
    @combustivel = Combustivel.new
    @response_combustivel = @combustivel.tipo_combustivel($autenticacao)
end

Entao('valido o retorno do servico tipos de combustivel') do
    expect(@response_combustivel.code).to eq(200)
end