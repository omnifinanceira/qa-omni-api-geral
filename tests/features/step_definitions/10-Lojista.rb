Dado('faco a requisicao de alterar seguros') do
  lojista = Lojista.new  
  @lista_lojista = lojista.altera_seguro($autenticacao)
end

Entao('valido o retorno da alterar seguros') do
  expect(lista_lojista.code).to eq(200)

end

Dado('faco a requisicao de Lista Operador') do
  lojista = Lojista.new 
  @lista_lojista = lojista.lojista_operador($autenticacao)
end

Entao('valido o retorno da Lista Operador') do
  expect(@lista_lojista.code).to eq(200)
end

Dado('faco a requisicao de Lista proposta agente') do
  lojista = Lojista.new 
  @lista_lojista = lojista.proposta_agente($autenticacao)
end

Entao('valido o retorno da Lista proposta agente') do
  expect(@lista_lojista.code).to eq(200)
end