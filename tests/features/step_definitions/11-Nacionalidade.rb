Dado('faco a requisicao da nacionalidade') do
    nacionalidade = Nacionalidade.new  
    @lista_nacionalidade = nacionalidade.lista_nacionalidade($autenticacao)  
end
  
Entao('valido o retorno da nacionalidade') do
    expect(@lista_nacionalidade.code).to eq(200)
 
end
  