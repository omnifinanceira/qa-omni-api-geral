Quando('faco a requisicao do servico de classe profissional') do
    seg = ClasseProfissional.new
    @lista_cp = seg.listar_classe_profissional($autenticacao)
end

Entao('valido o retorno do servico de classe profissional') do
    expect(@lista_cp.code).to eq(200)
end