Quando('faco a requisicao para Grupo Assistencia') do
    grupo = GrupoAPI.new
    @listar_grupo = grupo.listar_grupo($autenticacao)
end

Entao('valido o retorno Grupo Assistencia') do
    expect(@listar_grupo.code).to eq(200)
 end

