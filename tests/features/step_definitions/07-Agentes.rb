Quando('faco a requisicao para listar os agentes') do
    @agentes = Agentes.new
    @response_agentes = @agentes.listar_agentes($autenticacao)
end

Entao('valido o retorno do servico de agentes') do
    expect(@response_agentes.code).to eq(200)
end