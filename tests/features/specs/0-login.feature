#language:pt
@0 @login
@teste1
Funcionalidade: Login no Omni Mais

@login-agente-unico
Cenario: Realizar login com sucesso

Quando eu preencher o campo <usuario>
    E o campo <senha>
    E faco a requisicao
    Entao valido o retorno da autenticacao de agente unico

Exemplos:
    |usuario   |senha     |
    |"331LUANA"|"SENHA123"|

@login-agente-mais
Cenario: Realizar login com sucesso

Quando eu preencher o campo <usuario>
    E o campo <senha>
    E faco a requisicao do ws auth user
    Entao valido o retorno da autenticacao de mais de um agente

Exemplos:
    |usuario   |senha     |
    |"331LUANA"|"SENHA123"|
    # |"184BATISTELLA"|"SENHA123"|

@autenticar
Cenario: Autenticar

Dado que eu autentico